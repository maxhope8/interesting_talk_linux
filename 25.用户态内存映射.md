## 我的笔记

1. 问题引入

   - 用户态内存映射
2. mmap原理
   - 每个进程都有一个vm_area_struct指向虚拟地址空间的不同内存块, 这个变量叫做mmap
   - 内存映射:
     - 物理内存和虚拟地址之间的映射
     - 文件中的内容映射到虚拟内存空间 
   - 分配小内存使用brk, 大内存使用mmap
   - 映射文件
     - 需要文件的fd文件描述符, 并通过其得到文件对象本身
     - 调用get_unmapped_area找到一个没有映射的区域 
     - 调用mmap_region映射这个区域 
   - get_unmapped_area
     - 匿名映射
       - 在红黑树上找对之前的虚拟内存区域, 在这个区域后面建立新的虚拟内存区域
     - 文件映射
       - 找到文件对象, 通过文件对象本身定义的属相和操作来进行获取未映射区域
   - mmap_region
     - 首先尝试和前一个vm_area_struct 虚拟内存区域进行合并
     - 如果不可以需要创建一个新的vm_area_struct , 设置起始位置和结束位置, 加入队列
     - 如果是映射到文件, 则设置vm_file为目标文件, 也就是说将我们将vm_area_struct 的内存操作设置为了文件系统的操作, 读内存其实就是读写文件
     - 将新的vm_area_struct 挂载mm_struct红黑树上, 此时完成从内存到文件的映射
     - 文件到内存的映射通过vma_link, 对于打开的文件, 他有一个成员address_space , 他里面有一颗i_mmap的红黑树,  vm_area_struct 就挂到这颗树上, 此时文件到内存的映射完成
     - 但是还没有和物理内存发生任何关系, 因为只有当真正使用时才有操作物理内存
3. 用户态缺页异常
   - 访问某个虚拟地址, 如果没有对应的物理页, 就会出发缺页中断, 调用do_page_fault 方法
   - 判断缺页中断是否发生在内核, 如果发生在内核则调用vmalloc_fault ,内核里面，vmalloc区域需要内核页表映射到物理页 
   - pgd_t 用于全局页目录项，pud_t 用于上层页目录项，pmd_t 用于中间页目录项，pte_t 用于直接页表项 
   - 每个进程都有独立的页表, 顶级pgd存放在task_struct中的mm_struct的pgd变量里面 
   - 创建一个新的mm_struct时, 会调用mm_init->mm_alloc_pgd, 分配全局页目录项, 赋值给mm_struct的pgd成员变量
   - CPU真正对内存访问时, 调用context_switch进行上下文切换,  切换过程会调用load_new_mm_cr3 , cr3是一个寄存器, 指向进程顶级pgd, 当需要访问进程的虚拟内存,  自动从cr3找到pgd的物理地址, 根据页表解析虚拟内存地址为物理内存, 从而完成物理内存数据访问
   - cr3里面存放当前进程的顶级pgd，这个是硬件的要求 
   - cr3里面需要存放pgd在物理内存的地址，不能是虚拟地址 
   - 用户进程访问虚拟内存数据, 通过cr3转换成物理内存地址, 这个过程是在用户态运行的, 地址转换不需要进入内核态; 只有当访问虚拟内存时, 发现没有映射物理内存, 页表也没有创建过, 才会触发缺页异常
   - 1) 如果PTE，也就是页表项，从来没有出现过，那就是新映射的页 , 匿名页 属于这种情况
   - 2) 映射到文件, 调用do_fault, 属于第二种情况; 如果PTE存在过, 说明是之前换出的页, 应该换回来的页
     - kmap_atomic ,将物理内存映射到内核的虚拟地址空间，得到内核中的地址kaddr ; 本来地址转换不需要通过内核, 但是因为需要将数据读取写入物理页面, 但是又不能使用物理地址, 只能在内核中操作了
   - 3)do_swap_page, 用的换入, 不用的换出
     - 检查swap文件有没有缓存页, 没有则将swap文件读取内存, 通过mk_pte生成页表项, 将页表项插入页表, 清理掉swap文件
   - 页表中的映射关系建立好的之后,  用户程序在虚拟内存空间里, 就可以直接通过虚拟地址顺利经过页表访问物理页面的数据, 同时为了加速映射, 不想每次都到页表来获取, 对应关系, 但是页表又很大, 此时引入**TLB**（Translation Lookaside Buffer, 专门用来做地址映射, 他不在内存中而是和cpu在一起, TLB可以暂存部分常用的映射关系信息, 如果在TLB中找不到才会去内存中通过页表查询映射关系

### 大佬的评论

```python
why

12
- 申请小块内存用 brk; 申请大块内存或文件映射用 mmap
- mmap 映射文件, 由 fd 得到 struct file
- 调用 ...->do_mmap
- 调用 get_unmapped_area 找到一个可以进行映射的 vm_area_struct
- 调用 mmap_region 进行映射
- get_unmapped_area
- 匿名映射: 找到前一个 vm_area_struct
- 文件映射: 调用 file 中 file_operations 文件的相关操作, 最终也会调用到 get_unmapped_area
- mmap_region
- 通过 vm_area_struct 判断, 能否基于现有的块扩展(调用 vma_merge)
- 若不能, 调用 kmem_cache_alloc 在 slub 中得到一个 vm_area_struct 并进行设置
- 若是文件映射: 则调用 file_operations 的 mmap 将 vm_area_struct 的内存操作设置为文件系统对应操作(读写内存就是读写文件系统)
- 通过 vma_link 将 vm_area_struct 插入红黑树
- 若是文件映射, 调用 __vma_link_file 建立文件到内存的反映射
- 内存管理不直接分配内存, 在使用时才分配
- 用户态缺页异常, 触发缺页中断, 调用 do_page_default
- __do_page_fault 判断中断是否发生在内核
- 若发生在内核, 调用 vmalloc_fault, 使用内核页表进行映射
- 若不是, 找到对应 vm_area_struct 调用 handle_mm_fault
- 得到多级页表地址 pgd 等
- pgd 存在 task_struct.mm_struct.pgd 中
- 全局页目录项 pgd 在创建进程 task_struct 时创建并初始化, 会调用 pgd_ctor 拷贝内核页表到进程的页表
- 进程被调度运行时, 通过 switch_mm_irqs_off->load_new_mm_cr3 切换内存上下文
- cr3 是 cpu 寄存器, 存储进程 pgd 的物理地址(load_new_mm_cr3 加载时通过直接内存映射进行转换)
- cpu 访问进程虚拟内存时, 从 cr3 得到 pgd 页表, 最后得到进程访问的物理地址
- 进程地址转换发生在用户态, 缺页时才进入内核态(调用__handle_mm_fault)
- __handle_mm_fault 调用 pud_alloc, pmd_alloc, handle_pte_fault 分配页表项
- 若不存在 pte
- 匿名页: 调用 do_anonymous_page 分配物理页 ①
- 文件映射: 调用 do_fault ②
- 若存在 pte, 调用 do_swap_page 换入内存 ③
- ① 为匿名页分配内存
- 调用 pte_alloc 分配 pte 页表项
- 调用 ...->__alloc_pages_nodemask 分配物理页
- mk_pte 页表项指向物理页; set_pte_at 插入页表项
- ② 为文件映射分配内存 __do_fault
- 以 ext4 为例, 调用 ext4_file_fault->filemap_fault
- 文件映射一般有物理页作为缓存 find_get_page 找缓存页
- 若有缓存页, 调用函数预读数据到内存
- 若无缓存页, 调用 page_cache_read 分配一个, 加入 lru 队列, 调用 readpage 读数据: 调用 kmap_atomic 将物理内存映射到内核临时映射空间, 由内核读取文件, 再调用 kunmap_atomic 解映射
- ③ do_swap_page
- 先检查对应 swap 有没有缓存页
- 没有, 读入 swap 文件(也是调用 readpage)
- 调用 mk_pte; set_pet_at; swap_free(清理 swap)
- 避免每次都需要经过页表(存再内存中)访问内存
- TLB 缓存部分页表项的副本
```
文章: https://time.geekbang.org/column/article/97030