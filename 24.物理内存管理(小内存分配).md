## 我的笔记

1. 问题引入

   - 物理内存管理(小内存分配)
2. 小内存分配
   - 创建进程的时候，会调用dup_task_struct，它想要试图复制一个task_struct对象，需要先调用alloc_task_struct_node，分配一个task_struct对象 
   - 调用了kmem_cache_alloc_node函数，在task_struct的缓存区域task_struct_cachep分配了一块内存 
   - 系统初始化时, task_struct_cachep会被kmem_cache_create函数创建. 他专门用于和分配task_struct对象的缓存, 缓存区的名字叫做task_struct. 缓存区中每块的大小正好等于task_struct的大小arch_task_struct_size 
   - 每次创建task_struct结构, 不需要去内存中分配, 直接从缓存查看是否有可以用的
   - 当进程结束, task_struct不会直接销毁, 而是放回缓存中, 等待下次调用
3. kmem_cache缓冲区结构
   - list_head: 在Linux中要创建和管理的缓存不只是task_struct, 还有mm_struct, fs_struct, 一次所有缓存都会存放在一个链表中也就是list_head
   - 对于缓存来说, 其实就是分配几个连续的大内存块, 然后根据缓存对象的大小, 切成小的内存块
   - size: 包含这个指针的大小
   - object_size: 就是对象的实际大小
   - offset: 县一个空闲对象的指针存放在当前项的偏移量
4. kmem_cache_cpu和kmem_cache_node 
   - 缓存块分配有两种方式fast path 和slow path, 也就是**快速通道**(kmem_cache_cpu )和**普通通道** (kmem_cache_node )
   - 每次分配先从kmem_cache_cpu 取, 取不到就去kmem_cache_node 取, 还是取不到就找伙伴系统分配新的页
   - kmem_cache_cpu
     - page: 执行大内存块的第一个页
     - freelist: 执行大内存快的第一个空闲的项, 同时会有一个指针指向下一个空闲的项, 所有的空闲项形成一个链表
     - partial: 也指向大内存的第一个页, 但是其中的部分空间被分配出去了
   - kmem_cache_node 
     - partial: 是一个链表, 存放部分空闲的大块内存, 是kmem_cache_cpu中partial的备用链表
   - kmem_cache_alloc_node 会调用slab_alloc_node , 取出kmem_cache_cpu 的freelist, 第一个空闲项,  有则返回, 没有则进入普通通道; 首先再次尝试从kmem_cache_cpu 的freelist取, 因为可能有进程释放了缓存, 找到就返回;否则先看kmem_cache_cpu 的partial是否是空的, 如果不空就将kmem_cache_cpu 的page替换为partial的大块内存; 如果还不行就会获取kmem_cache_node开始才kmem_cache_node 的partial链表上拿取大块内存t,同时将freelist赋值给取到的块t; 然后将kmem_cache_cpu 的page指向取到的块上,返回取到的t. 此时如果kmem_cache_cpu 也有一个partial就会进行第二次循环, 再次去一个块, 放到kmem_cache_cpu 的partial里
   - 如果kmem_cache_node里面也没有空闲的内存, 说明原来分配的页放满了,  需要分配新的页面给kmem_cache_node, 如果分配还不成功, 说明内存不够用了, 此时会申请更小的内存页
5. 页面换出
   - 原因: 内存不可能将磁盘上的数据都存储, 此时就涉及到那些数据要被缓存, 哪些数据要被淘汰
   - 什么情况会触发页面换出?
     - 1) 分配内存时发现没有内存了
     - 2)内核线程**kswapd** , 一个操作系统启动就执行的函数, 如果内存充足就睡眠, 否则就进行换出内存页
   - 以内存节点为单位,  会维护一个lru列表, 将最近最少使用内存页, 换出到磁盘
   - 内存页分为两类: 匿名页-和虚拟地址空间进行关联; 内存映射-不但和虚拟地址空间挂链, 还和文件管理关联
   - 每个类都有两个列表: active(活跃内存页), inactive(不活跃内存页), 内存会随时发生变化从active->inactive或者从inactive->active

### 大佬的评论

```python
why

4
- 小内存分配, 例如分配 task_struct 对象
- 会调用 kmem_cache_alloc_node 函数, 从 task_struct 缓存区域 task_struct_cachep(在系统初始化时, 由 kmem_cache_create 创建) 分配一块内存
- 使用 task_struct 完毕后, 调用 kmem_cache_free 回收到缓存池中
- struct kmem_cache 用于表示缓存区信息, 缓存区即分配连续几个页的大块内存, 再切成小内存
- 小内存即缓存区的每一项, 都由对象和指向下一项空闲小内存的指针组成(随机插入/删除+快速查找空闲)
- struct kmem_cache 中三个 kmem_cache_order_objects 表示不同的需要分配的内存块大小的阶数和对象数
- 分配缓存的小内存块由两个路径 fast path 和 slow path , 分别对应 struct kmem_cache 中的 kmem_cache_cpu 和 kmem_cache_node
- 分配时先从 kmem_cache_cpu 分配, 若其无空闲, 再从 kmem_cache_node 分配, 还没有就从伙伴系统申请新内存块
- struct kmem_cache_cpu 中
- page 指向大内存块的第一个页
- freelist 指向大内存块中第一个空闲项
- partial 指向另一个大内存块的第一个页, 但该内存块有部分已分配出去, 当 page 满后, 在 partial 中找
- struct kmem_cache_node
- 也有 partial, 是一个链表, 存放部分空闲的多个大内存块, 若 kmem_cacche_cpu 中的 partial 也无空闲, 则在这找
- 分配过程
- kmem_cache_alloc_node->slab_alloc_node
- 快速通道, 取出 kmem_cache_cpu 的 freelist , 若有空闲直接返回
- 普通通道, 若 freelist 无空闲, 调用 `__slab_alloc`
- `__slab_alloc` 会重新查看 freelist, 若还不满足, 查看 kmem_cache_cpu 的 partial
- 若 partial 不为空, 用其替换 page, 并重新检查是否有空闲
- 若还是无空闲, 调用 new_slab_objects
- new_slab_objects 根据节点 id 找到对应 kmem_cache_node , 调用 get_partial_node
- 首先从 kmem_cache_node 的 partial 链表拿下一大块内存, 替换 kmem_cache_cpu 的 page, 再取一块替换 kmem_cache_cpu 的 partial
- 若 kmem_cache_node 也没有空闲, 则在 new_slab_objects 中调用 new_slab->allocate_slab->alloc_slab_page 根据某个 kmem_cache_order_objects 设置申请大块内存
- 页面换出
- 触发换出:
- 1) 分配内存时发现没有空闲; 调用 `get_page_from_freelist->node_reclaim->__node_reclaim->shrink_node`
- 2) 内存管理主动换出, 由内核线程 kswapd 实现
- kswapd 在内存不紧张时休眠, 在内存紧张时检测内存 调用 balance_pgdat->kswapd_shrink_node->shrink_node
- 页面都挂在 lru 链表中, 页面有两种类型: 匿名页; 文件内存映射页
- 每一类有两个列表: active 和 inactive 列表
- 要换出时, 从 inactive 列表中找到最不活跃的页换出
- 更新列表, shrink_list 先缩减 active 列表, 再缩减不活跃列表
- 缩减不活跃列表时对页面进行回收:
- 匿名页回收: 分配 swap, 将内存也写入文件系统
- 文件内存映射页: 将内存中的文件修改写入文件中
```

```python
周平
老师，有个疑问，这些内存小块的分配及释放，都是在物理内存中完成的呢，还是在虚拟内存中完成再映射对对应的物理内存呢？
2019-05-27
作者回复
物理内存分配完毕一整页的时候，会通过page_address分配一个虚拟地址，小块都是基于这个虚拟地址的。
```

```python
chengzise

0
老师好，这两节讲的是物理内存的管理和分配，大概逻辑是看的懂的，细节需要继续反复研读才行。其中有个问题，kmem_cache部分已经属于页内小内存分配，这个分配算法属于虚拟内存分配，不算物理内存管理范畴吧？ 就是说先分配物理内存页，映射到虚拟内存空间，再在这个虚拟内存分配小内存给程序逻辑使用。希望老师解答一下。
2019-05-23
 作者回复
是的，会映射成为虚拟地址，是整页会有虚拟地址
```

文章: https://time.geekbang.org/column/article/96623